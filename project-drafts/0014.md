---
id: "0014"
datePosted: "2022-09-29"
title: "Create a Self-Minting Contract for Contributor Tokens"
lovelace: 250000000
gimbals: 5000
status: "Coming Soon"
devCategory: "Contributor Token"
bbk: [""]
approvalProcess: 4
multipleCommitments: false
repositoryLink: "https://gitlab.com/gimbalabs/plutus-pbl-summer-2022/projects/GPTE/GPTE-plutus"
---

## Outcome:

## Requirements:

## How To Start:
- First, you'll have to decide on the rules!

## Links + Tips:
[gitlab issue](https://gitlab.com/gimbalabs/plutus-pbl-summer-2022/projects/GPTE/GPTE-front-end/-/issues/7)
## How To Complete:
