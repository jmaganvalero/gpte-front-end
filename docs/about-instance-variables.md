## How to configure an instance of GPTE Front End:

### `/cardano/plutus`
- `escrowContract.ts` has the `address` and `script` for [EscrowValidator]()
- `treasuryContract.ts` includes `address` and `script` for [TreasuryValidator](), along with all additional project variables: tokens, metadatakey, and Treasury Datum.
- [ ] Add notes on each of these variables?

### `apollo-client.ts`
- Simply comment in/out the appropriate GraphQL endpoint you need, depending on the Cardano network in use.

### Dividing by `1000000` on Mainnet vs. Testnet
Currently in which files:
- `gpte-front-end/components/treasury/TreasurySummary.tsx`
- `gpte-front-end/components/transactions/commitToProject.tsx`
- [ ]: Will be better to make this a variable in `treasuryContract.ts`