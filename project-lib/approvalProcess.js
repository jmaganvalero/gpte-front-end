export const approvalProcess = [
    {
        "number": 1,
        "name": "Direct from Issuer",
        "description": "Issuer decides whether Project is completed, then Distributes it."
    },
    {
        "number": 2,
        "name": "Git contribution approved by Issuer",
        "description": "Submit a Merge Request to this project on GitLab. When Approved, Commitment will be distributed."
    },
    {
        "number": 3,
        "name": "Async Group Approval",
        "description": "Share results with N people and get approval from M of N. Key question: Who can be in the list of N? We need a system for identifying who can be the Approver. We can take a look at Peer Review processes."
    },
    {
        "number": 4,
        "name": "Live Group Consent",
        "description": "Give a live presentation at Playground or at Live Coding and take feedback. The Project Commitment will be distributed by a consent process, which means that no one in the meeting has an objection to distribution."
    },
    {
        "number": 5,
        "name": "Live Group Approval",
        "description": "Give a live presentation at Playground or at Live Coding, get approval from M of N attendees."
    },
]
