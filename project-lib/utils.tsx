import { UTxO, Asset } from "@meshsdk/core";
import {
  EscrowTx,
  ProjectStatus,
  escrowTxFromMetadataKey,
  ProjectTxMetadata,
  ContributorHistory,
} from "../types";
import { escrow } from "../cardano/plutus/escrowContract";
import { hexToString } from "../cardano/utils";
import { treasury } from "../cardano/plutus/treasuryContract";

// Data tracking utility functions. Should this file have a different name?

export function getAllEscrowTransactions(transactionData: {
  transactions: escrowTxFromMetadataKey[];
}): EscrowTx[] {
  const txs: EscrowTx[] = [];
  const wrongTxMetadata: String[] = [
    "8becda7df9672b41f11d56b82dbb3af414aff827369078ac66463ce3507d5312",
    "bcf10dfda3012aca1574bdcdd11ac6e51e000d08392e455857fff90f6daec148",
    "a6ea1b7f2c96db8f097534a17f45cfa8934c9e9d70a0737b0ad90d7549de2724",
    "2b54d1fca409af48ee4a23981f34f15af09177d66b5423ab80a8102cfb3a68db",
    "6a790236f4b97bce8c33b97e8090ec830015a2afced6631a442a14831e4f50a5",
  ];
  transactionData.transactions.forEach(
    (element: escrowTxFromMetadataKey) => {
      if (!wrongTxMetadata.includes(element.hash)) {
        
        const _includedAt = element.includedAt;

        const _metadata: ProjectTxMetadata = {
          // Upcoming Project: a more robust version of this would filter for the correct metadata key.
          // See escrowTxFromMetadataKey type.
          id: element.metadata[0].value.id,
          hash: element.metadata[0].value.hash,
          txType: element.metadata[0].value.txType,
          expTime: element.metadata[0].value.expTime,
          contributor: element.metadata[0].value.contributor,
        };

        const _inputs: UTxO[] = [];
        const _outputs: UTxO[] = [];

        element.inputs.forEach((i: any) => {
          const lovelace = i.value;
          // start asset array
          const _amount: Asset[] = [
            {
              unit: "lovelace",
              quantity: lovelace,
            },
          ];
          i.tokens.forEach((val: any) => {
            _amount.push({
              unit: val.asset.assetId,
              quantity: val.quantity,
            });
          });
          const _utxo: UTxO = {
            input: {
              txHash: i.txHash,
              outputIndex: i.sourceTxIndex,
            },
            output: {
              address: i.address,
              amount: _amount,
            },
          };
          _inputs.push(_utxo);
        });

        element.outputs.forEach((out: any) => {
          const lovelace = out.value;
          // start asset array
          const _amount: Asset[] = [
            {
              unit: "lovelace",
              quantity: lovelace,
            },
          ];
          out.tokens.forEach((val: any) => {
            _amount.push({
              unit: val.asset.assetId,
              quantity: val.quantity,
            });
          });
          const _utxo: UTxO = {
            input: {
              txHash: out.txHash,
              outputIndex: out.index,
            },
            output: {
              address: out.address,
              amount: _amount,
            },
          };
          _outputs.push(_utxo);
        });

        let _contributorTokenName: string = "";
        // If Contribution get Contrib token by looking at output to Escrow
        if (_metadata.txType === "Commitment") {
          const toEscrow: Asset[] = _outputs.filter(
            (output) => output.output.address === escrow.address
          )[0].output.amount;
          console.log("Check", toEscrow);
          const assetId: string = toEscrow.filter(
            (asset) =>
              asset.unit.substring(0, 56) ==
              treasury.accessTokenPolicyId
          )[0].unit;
          if (assetId) {
            _contributorTokenName = hexToString(assetId.substring(56));
          }
        }

        // If Distribution get Contrib token by looking at input from Escrow
        if (_metadata.txType === "Distribute") {
          const fromEscrow: Asset[] = _inputs.filter(
            (input) => input.output.address === escrow.address
          )[0].output.amount;
          const assetId: string = fromEscrow.filter(
            (asset) =>
              asset.unit.substring(0, 56) ==
              treasury.accessTokenPolicyId
          )[0].unit;
          _contributorTokenName = hexToString(assetId.substring(56));
        }

        const _utxo: EscrowTx = {
          includedAt: _includedAt,
          metadata: _metadata,
          inputs: _inputs,
          outputs: _outputs,
          type: _metadata.txType,
          contributorTokenName: _contributorTokenName,
        };

        txs.push(_utxo);
      }
    }
  );
  return txs;
}

export function getAllProjectStatus(
  transactionData: EscrowTx[]
): ProjectStatus[] {
  const result: ProjectStatus[] = [];

  // Next step: if id exists in result, then edit that record
  transactionData.forEach((tx: EscrowTx) => {
    if (result.find((b) => b.id === tx.metadata.id)) {
      for (const status of result) {
        if (status.id === tx.metadata.id) {
          status.hash.push(tx.metadata.hash);
          status.transactions.push(tx);
        }
      }
    } else {
      const _project = {
        id: tx.metadata.id,
        hash: [tx.metadata.hash],
        transactions: [tx],
      };
      result.push(_project);
    }
  });

  return result;
}

export function getAllContributorTokenStatus(
  transactionData: EscrowTx[]
): ContributorHistory[] {
  const result: ContributorHistory[] = [];

  transactionData.forEach((tx: EscrowTx) => {
    if (
      result.find(
        (contributor) =>
          contributor.contributorTokenName === tx.contributorTokenName
      )
    ) {
      for (const contrib of result) {
        if (contrib.contributorTokenName === tx.contributorTokenName) {
          contrib.transactions.push(tx);
        }
      }
    } else {
      const _contributor = {
        contributorTokenName: tx.contributorTokenName,
        transactions: [tx],
      };
      result.push(_contributor);
    }
  });

  return result;
}
