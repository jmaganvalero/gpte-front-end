---
id: "0018"
datePosted: "2022-09-30"
title: "Extract a Project Details Component"
lovelace: 25000000
gimbals: 500
status: "Open"
devCategory: "Front End"
bbk: [""]
approvalProcess: 2
multipleCommitments: false
repositoryLink: "https://gitlab.com/gimbalabs/plutus-pbl-summer-2022/projects/GPTE/GPTE-front-end"
---

## Outcome:
- On [/Projects/[id].tsx](https://gitlab.com/gimbalabs/plutus-pbl-summer-2022/projects/GPTE/GPTE-front-end/-/tree/main/pages/Projects) the Project Details Component is in a separate file.

## Requirements:
- The new component should appear on the right side of the browser window, below the `<ContributorTokens />` component.
- Style it however you'd like! This is another example of a Component like those described in Projects 0001 and 0002.

## How To Start:
- Open [/Projects/[id].tsx](https://gitlab.com/gimbalabs/plutus-pbl-summer-2022/projects/GPTE/GPTE-front-end/-/tree/main/pages/Projects) and search for "Project 0018"
- Review the code for `<ContributorTokens />` and `<CommitToProject ProjectData={ProjectData} />`.

## Links + Tips:
- You will need to pass `ProjectData` to your new component. Follow the pattern in `CommitToProject`.

## How To Complete:
- Give your a component a unique and descriptive name.
- Submit a Merge Request to [GPTE-front-end](https://gitlab.com/gimbalabs/plutus-pbl-summer-2022/projects/GPTE/GPTE-front-end) with your new component in the [components/Project directory](https://gitlab.com/gimbalabs/plutus-pbl-summer-2022/projects/GPTE/GPTE-front-end/-/tree/main/components/Project)