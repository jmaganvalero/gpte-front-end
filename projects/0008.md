---
id: "0008"
datePosted: "2022-09-29"
title: "Create an Interface for Issuer to Distribute Completed Projects"
lovelace: 200000000
gimbals: 4000
status: "Open"
devCategory: "Front End"
bbk: [""]
approvalProcess: 4
multipleCommitments: false
repositoryLink: "https://gitlab.com/gimbalabs/plutus-pbl-summer-2022/projects/GPTE/GPTE-front-end"
---

## Outcome:
- Build a front-end interface for Treasury Issuers to Distribute completed Projects from the Escrow Contract.

## Requirements:
- Create a new page at localhost:3000/distribute.
- For the Issuer, this page should display some all open Projects, each with a Button that triggers the Distribution Transaction.
- For all other visitors, this page should only display all open Projects, but no Distribution Buttons.
- [See prior version for an example]() and the [source code]()

## How To Start:
- As with Project 0007, you will need to create your own instance of GPTE in order to test your implementation of `/distribute`.
- Your instance will need to use PubKeyHash of an Issuer using a Browser Wallet like Nami or Eternl. You can get the PubKeyHash of a an address like this: [https://gitlab.com/gimbalabs/plutus-pbl-summer-2022/ppbl-front-end-template/-/blob/main/pages/utilities/working-with-pubkeyhash/index.tsx](https://gitlab.com/gimbalabs/plutus-pbl-summer-2022/ppbl-front-end-template/-/blob/main/pages/utilities/working-with-pubkeyhash/index.tsx).
- When you have an instance of GPTE running, you'll be able to test your new component.

## Links + Tips:
- Look at [the 03-issuer-distributes-Project script in GPTE-plutus](https://gitlab.com/gimbalabs/plutus-pbl-summer-2022/projects/GPTE/GPTE-plutus/-/blob/master/scripts/03-issuer-distributes-Project.sh). Your task is to recreate this transaction in the web browser, using a Browser Wallet.
- This Transaction interacts with only the Project Contract.

## How To Complete:
- Submit a Merge Request to [GPTE-front-end](https://gitlab.com/gimbalabs/plutus-pbl-summer-2022/projects/GPTE/GPTE-front-end) with your new `/distribute` Page and any accompanying Components.