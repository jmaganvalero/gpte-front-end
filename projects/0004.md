---
id: "0004"
datePosted: "2022-11-02"
title: "Create a Modal for Confirming Commitment Tx"
lovelace: 50000000
gimbals: 1000
status: "Open"
devCategory: "Front End"
bbk: [""]
approvalProcess: 2
multipleCommitments: true
repositoryLink: "https://gitlab.com/gimbalabs/plutus-pbl-summer-2022/projects/GPTE/GPTE-front-end"
---

## Outcome:
Build a [Modal](https://chakra-ui.com/docs/components/modal/usage) for confirming the Project Commitment Transaction.

## Requirements:
- When a Contributor clicks the "Commit to Project" button, a [Modal](https://chakra-ui.com/docs/components/modal/usage) appears.
- The Modal displays relevant Commitment Transaction details and a Confirmation button.
- When the Contributor clicks the Confirmation button, the Commitment Transaction is initialized.

## How To Start:
- Read the [Chakra UI Modal documentation](https://chakra-ui.com/docs/components/modal/usage). Be sure to click the buttons in the Usage examples.
- Run [GPTE-front-end](https://gitlab.com/gimbalabs/plutus-pbl-summer-2022/projects/GPTE/GPTE-front-end) locally and navigate to [localhost:3000/Projects/0001](http://localhost:3000/Projects/0001) to see the context for this task.
- Open `/components/transactions/commitToProject.tsx` to make changes.

## Links + Tips:
- Currently, there is just one "Commit to Project" button. To complete this task, you will need two buttons with two unique `onClick` events. The first button will open the modal. When it opens, the modal will contain a Confirmation button. In the GPTE Front End repo, there's a branch called `modal-test` with a basic example if you need an extra nudge :)
- More generally, [useDisclosure](https://chakra-ui.com/docs/hooks/use-disclosure) is pretty cool to know about.
- You can also review the old gtbe to see a Modal in action: [look for Modal on line 231](https://gitlab.com/gimbalabs/gimbal-Project-treasury-and-escrow/gimbal-tracker-v1/-/blob/master/src/templates/ProjectPage.js). Note that this project was built on Gatsby, so the structure is a bit different.

## How To Complete:
- Note that just like Projects 0001 and 0002, there could be multiple styles for this modal. To submit your results for this one, please create a unique `commitToProject___.tsx` file. If you completed Project 0002, you can add these changes to to the component you built.
- Submit a Merge Request to [GPTE-front-end](https://gitlab.com/gimbalabs/plutus-pbl-summer-2022/projects/GPTE/GPTE-front-end) with your new/updated `commitToProject` component.