import {
  Box,
  Heading,
  Text,
  Link,
  Grid,
  GridItem,
  ListItem,
  UnorderedList,
  Button,
} from "@chakra-ui/react";
import { NextPage } from "next";
import Image from "next/image";
import { approvalProcess } from "../../project-lib/approvalProcess";

const HowItWorksPage: NextPage = () => {
  return (
    <Box bg="gray.800" color="white">
      <Grid gridTemplateColumns="repeat(6, 1fr)" gap="5">
        <GridItem colSpan={3} w="80%" mx="auto" p="5">
          <Heading pb="4" size="2xl">
            How GPTE Works
          </Heading>
          <Text py="4" fontSize="xl">
            Gimbal Project Treasury and Escrow (GPTE) is an{" "}
            <Link href="https://gitlab.com/gimbalabs/plutus-pbl-summer-2022/projects/gpte">
              open-source Cardano Dapp
            </Link>{" "}
            that allows builders to coordinate work and get projects done.
          </Text>
          <Text py="4" fontSize="xl">
            It consists of two Smart Contracts: A Treasury Contract that holds a
            pool of tokens, and an Escrow Contract that manages Commitments made
            by Contributors.
          </Text>
          <Text py="4" fontSize="xl">
            GPTE provides essential Cardano tooling that anyone can use. It
            provides rails for new forms of distributed collaboration on both
            long-term projects and quick tasks, paving the way for people to
            make the most of both their independence and interdependence in a
            decentralized world.
          </Text>
          <Text py="4" fontSize="xl">
            The Gimbalabs developer community is working to extend the features
            of this Decentralized Application, and is ready to support you to
            create your own implementation.
          </Text>
        </GridItem>
        <GridItem colSpan={3}>
          <Box
            w="fit-content"
            p="5"
            mx="auto"
            borderRadius="md"
            boxShadow="2xl"
          >
            <Image
              src="/gpteDiagram.png"
              width="1200"
              height="675"
              alt="gbte-visualization"
            />
          </Box>
        </GridItem>
        <GridItem
          colSpan={6}
          w="60%"
          mx="auto"
          my="10"
          p="5"
          bgGradient="linear(to-br, gray.700, gray.900)"
          borderRadius="lg"
          boxShadow="2xl"
        >
          <Heading size="2xl" py="5" textAlign="center">
            A Plutus-Enabled, Headless Dapp
          </Heading>
          <Text py="2" px="5" fontSize="xl" lineHeight="8">
            GPTE is built on two simple yet effective Plutus Contracts that help
            developers understand how Plutus works. Our goal is to help
            developers to imagine new use cases.
          </Text>
          <Text py="2" px="5" fontSize="xl" lineHeight="8">
            A Treasury Contract locks a pool of tokens and sets the rules for
            how Contributors commit to working on Projects.
          </Text>
          <Text py="2" px="5" fontSize="xl" lineHeight="8">
            A Commitment Escrow Contract locks a subset of tokens from the
            Treasury every time a Contibutor commits to a Project.
          </Text>
          <Text py="2" px="5" fontSize="xl" lineHeight="8">
            Thanks to the design of Plutus, the validation logic of these two
            contracts can be re-used across many instances of GPTE. Anyone who
            wants to start a Project can create a new, independent instance of
            the Dapp. Thanks to the architecture of Cardano, Contributors create
            an on-chain record of the Projects they complete, building emergent
            reputation that can be referenced by anyone.
          </Text>
          <Text py="2" px="5" fontSize="xl" lineHeight="8">
            Thanks to the promise headless dapps, anyone can create new user
            interfaces for interacting with these contracts and viewing data.
            And thanks to the transformative power of open source development,
            GPTE will change over time. We invite you to get involved by{" "}
            <Link href="https://gimbalabs.instructure.com/enroll/3CFNFB">
              signing up for Plutus PBL
            </Link>{" "}
            or{" "}
            <Link href="https://www.gimbalabs.com/calendar">
              dropping by Live Coding at Gimbalabs
            </Link>{" "}
            (every Wednesday and Thursday at 1430 UTC).
          </Text>
        </GridItem>
        <GridItem
          colSpan={6}
          w="60%"
          mx="auto"
          mb="10"
          p="5"
          bgGradient="linear(to-br, gray.700, gray.900)"
          borderRadius="lg"
          boxShadow="2xl"
        >
          <Heading size="2xl" py="5" textAlign="center">
            Tokenized Issuer and Contributor Roles
          </Heading>
          <Text py="2" textAlign="center" fontSize="2xl">
            To create an instance of GPTE, you will need to define two roles:
            Issuers and Contributors.
          </Text>
          <Heading pt="2" px="5">
            Issuers
          </Heading>
          <Text pb="5" px="5" fontSize="xl">
            lock funds in a treasury, write Project descriptions in Markdown
            files{" "}
            <Link href="https://gitlab.com/gimbalabs/plutus-pbl-summer-2022/projects/gpte/gpte-front-end/-/tree/main/projects">
              like this
            </Link>
            , and approve the distribution of Project tokens.
          </Text>
          <Heading pt="2" px="5">
            Contributors
          </Heading>
          <Text pb="5" px="5" fontSize="xl">
            make Commitments to working on Projects, and earn tokens upon
            completion. Unique Approval Processes can be defined for each
            instance of GPTE. At Gimbalabs we are actively experimenting with
            collective Approval Processes, and refining the Treasury and Escrow
            Contracts based on what we learn.
          </Text>
          <Text py="2" px="5" fontSize="xl">
            Issuer and Contributor Roles are represented by Cardano Native
            Assets. Issuer and Contributor Tokens can be used across different
            instances of GPTE, or can be minted with new rules for every unique
            implementation of GPTE.
          </Text>
        </GridItem>

        <GridItem py="10" colSpan={6}>
          <Heading size="4xl" py="5" textAlign="center">
            How do you want to build?
          </Heading>
        </GridItem>

        <GridItem
          p="5"
          mx="3"
          colSpan={2}
          bgGradient="linear(to-br, gray.700, gray.900)"
          borderRadius="lg"
          boxShadow="2xl"
        >
          <Heading size="2xl" pb="3">
            Developers
          </Heading>
          <Text fontSize="2xl" pb="3">
            Build Your Skills + Your Reputation
          </Text>
          <UnorderedList>
            <ListItem key="1" py="1" fontSize="xl">
              Learn to build with{" "}
              <Link
                href="https://gimbalabs.instructure.com/enroll/3CFNFB"
                target="_blank"
              >
                Plutus PBL
              </Link>
            </ListItem>
            <ListItem key="2" py="1" fontSize="xl">
              Earn a{" "}
              <Link href="https://gimbalabs.com/mastery-policy" target="_blank">
                Contributor Token
              </Link>
            </ListItem>
            <ListItem key="3" py="1" fontSize="xl">
              Contribute to{" "}
              <Link
                href="https://gitlab.com/gimbalabs/plutus-pbl-summer-2022/projects/gpte"
                target="_blank"
              >
                this project
              </Link>
            </ListItem>
            <ListItem key="4" py="1" fontSize="xl">
              Meet collaborators at{" "}
              <Link href="https://gimbalabs.com/calendar" target="_blank">
                weekly Live Coding sessions
              </Link>
            </ListItem>
          </UnorderedList>
        </GridItem>

        <GridItem
          p="5"
          mx="3"
          colSpan={2}
          bgGradient="linear(to-br, gray.700, gray.900)"
          borderRadius="lg"
          boxShadow="2xl"
        >
          <Heading size="2xl" pb="3">
            Project Managers
          </Heading>
          <Text fontSize="2xl" pb="3">
            Create an Instance of GPTE
          </Text>
          <UnorderedList>
            <ListItem key="1" py="1" fontSize="xl">
              Review{" "}
              <Link
                href="https://gitlab.com/gimbalabs/plutus-pbl-summer-2022/projects/gpte"
                target="_blank"
              >
                documentation
              </Link>
            </ListItem>
            <ListItem key="2" py="1" fontSize="xl">
              Join in at{" "}
              <Link href="https://gimbalabs.com/calendar" target="_blank">
                weekly Playground Meetings
              </Link>
            </ListItem>
            <ListItem key="3" py="1" fontSize="xl">
              Tutorials coming soon!
            </ListItem>
            <ListItem key="4" py="1" fontSize="xl">
              Need help{" "}
              <Link
                href="https://discord.com/invite/Va7DXqSSn8"
                target="_blank"
              >
                say hello on Gimbalabs Discord
              </Link>
            </ListItem>
          </UnorderedList>
        </GridItem>

        <GridItem
          p="5"
          mx="3"
          colSpan={2}
          bgGradient="linear(to-br, gray.700, gray.900)"
          borderRadius="lg"
          boxShadow="2xl"
        >
          <Heading size="xl" pb="3">
            Need your own GPTE Implementation?
          </Heading>
          <a href="https://form.typeform.com/to/Nua7UMSi">
            <Button my="5" colorScheme="green">
              Contact Us
            </Button>
          </a>
        </GridItem>

        <GridItem
          w="50%"
          mx="auto"
          my="10"
          p="5"
          colSpan={6}
          bgGradient="linear(to-br, gray.700, gray.900)"
          borderRadius="lg"
          boxShadow="2xl"
        >
          <Heading>Approval Processes</Heading>
          <Text py="2">
            A number indicating the required Approval Process is included in
            each Project description. We will test these processes and refine
            this list.
          </Text>
          {approvalProcess.map((i) => (
            <>
              <Text pt="3" fontWeight="bold">
                {i.number}: {i.name}
              </Text>
              <Text pt="1">{i.description}</Text>
            </>
          ))}
        </GridItem>
      </Grid>
    </Box>
  );
};

export default HowItWorksPage;
