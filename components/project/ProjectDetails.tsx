import {Box,Heading,Text,Stack,Badge, Center} from "@chakra-ui/react";
  import { Project } from "../../types";
 
type Props = {
projectData: Project;
};

const ProjectDetails: React.FC<Props> = ({ projectData }) => {
return (
<Box mb="5" p="5" bgGradient="linear(to-br, gray.700, gray.900)" borderRadius="lg" boxShadow="2xl">
    <Heading pb="2">Project Details</Heading>
    <Heading size="md" pb="1">Project ID</Heading>
    <Text pb="2">{projectData.id}</Text>
    <Heading size="md" pb="1">Project Hash (experimental)</Heading>
    <Text pb="2">{projectData.projectHash}</Text>
    
    <Stack direction='row'>
        <Badge variant='outline' colorScheme='green'>
        <Text>Ada: {projectData.lovelace / 1000000}</Text>
        </Badge>
        <Badge variant='outline' colorScheme='green'>
            <Text>Gimbals: {projectData.gimbals}</Text>
        </Badge>
    </Stack>
  </Box>
  )
}

export default ProjectDetails;