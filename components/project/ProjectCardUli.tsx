import Link from "next/link";
import { EscrowTx, Project, rawTxEscrow } from "../../types";
import { FaGitlab } from "react-icons/fa";
import { ExternalLinkIcon } from '@chakra-ui/icons';
import theme from "../../themes/uli/themeUli";

import {
  Button,
  Box,
  Heading,
  Text,
  Center,
  Grid,
  GridItem,
  Link as ChakraLink,
  ChakraProvider,
} from "@chakra-ui/react";

import CardBox from "../../themes/uli/CardBox";
import CardTag from "../../themes/uli/CardTag";

import { filterTransactionsById, getContributorTokenNames, getCommitmentStatus } from "../tracking/CommitmentStatus";

type Props = {
  project: Project;
  txToEscrow: rawTxEscrow[];
  txFromEscrow: rawTxEscrow[];
};

const ProjectCard: React.FC<Props> = ({ project: p, txToEscrow: txT, txFromEscrow: txF  }) => {

  const txToEscrow = filterTransactionsById(txT, p.id);
  const txFromEscrow = filterTransactionsById(txF, p.id);
  let status = getCommitmentStatus(getContributorTokenNames(txToEscrow, "to"),getContributorTokenNames(txFromEscrow, "from"), p.multipleCommitments)
  let statusText = status[0] == "Open for Commitment" ? "Open" : "Closed";
  let statusColor = status[0] == "Open for Commitment" ? "green.600" : "red.600";
  let projectColor = getProjectColor(p.devCategory);
  return (
    <ChakraProvider theme={theme}>
    <GridItem
      bgGradient='linear(to-r, gray.500, gray.400, gray.500)'
      key={p.id}
      border="4px"
      borderColor={projectColor}
      borderRadius="md"
    >
      <Box>
      <CardBox noOfLines={2} height="70px">
      <Heading size="md" color="black" textAlign="center">{p.title}</Heading>
        </CardBox>
        <CardBox>
          <Text fontWeight="bold" color='Black'>Card: {p.id}</Text><Text fontWeight="bold" color='Black'>Created: {p.datePosted}</Text>
        </CardBox>
        <Box p="2">
          <Grid templateColumns="repeat(2, 3fr)" gap="5" pb="5">
            <CardTag  bg="blue.600">{p.gimbals} gimbals</CardTag><CardTag bg="blue.600"> {p.lovelace / 1000000} ada</CardTag>
            <CardTag bg={projectColor}>{p.devCategory}</CardTag><CardTag bg={statusColor}>{statusText}</CardTag>
            <Button colorScheme='github' leftIcon={<FaGitlab />}>
              <ChakraLink href={p.repositoryLink} isExternal>
                  Gitlab <ExternalLinkIcon mx='2px' />
              </ChakraLink>
            </Button>
            <Link href={`/projects/${p.id}`}>
              <Center
              border="2px"
                py="3"
                bg="blue.800"
                borderRadius="full"
                color= "whiteAlpha.900"
                _hover={{cursor: "pointer", bgGradient:'linear(to-r, blue.800, blue.600, blue.800)'}}
              >View Details</Center>
            </Link>
          </Grid>
          </Box>
        </Box>
     </GridItem>
    </ChakraProvider>
  );
};

function getProjectColor(devCategory?: string | undefined): string {
  let projectColor = "gray.700";
    switch (devCategory){
      case("Data"):
        projectColor= "blue.900";
        break;
        case("Documentation"):
          projectColor= "teal.900";
          break;
        case("Education"):
          projectColor= "green.900";
          break;
        case("Front End"):
          projectColor= "pink.900";
          break;
        case("Plutus"):
          projectColor= "purple.900";
          break;
        default:
          projectColor = "gray.700";
      };
      return projectColor;
  }


export default ProjectCard;
