import { useQuery, gql } from "@apollo/client";
import { Box, Heading, Text, Center, Spinner } from "@chakra-ui/react";

import { escrow } from "../../cardano/plutus/escrowContract";

/*
This component is meant to be an example to show how GraphQL queries can be 
implemented relying on datums and redeemers instead of metadata.

In the first GPTE design, commitments and distribution transactions are tracked
using metadata. This choice was made to make the implementation easy and more 
understandable to contributors. It also makes debugging easier in a testing
enviroment, but comes with some disadvantages, especially when used on the main
chain. 
It's considered best practice to not pollute the valueable on-chain space with
redundant information such as adding data already present, for example inside
datums and redeemers. This results in higher transaction fees, undesirable for
a deployment on mainnet
*/

const TRANSACTION_FROM_ESCROW_REDEEMERS_QUERY = gql`
query TransactionsFromEscrowRedeemers($escrowAddress: String!) {
  transactions(where: { inputs: { address: { _eq: $escrowAddress } } } ) {
    hash
    inputs {
      address 
      redeemer {
        datum {
          bytes
        }
      }
    }
  }
}
`;


function countDistributionTransaction(transactions: any) {

  //  The escrow contract validator redeemer is of type:
  //  data ProjectAction = Cancel | Distribute | Update
  //  "d87a80" is the byte representation of the BuiltInData "{ constructor: 1, fields: [] }"
  //  corresponding to the Distribute action
  const distributeRedeemerBytes = "d87a80"
  let distributionTxs = 0

  transactions.forEach((tx: any) => {
    tx.inputs.forEach((input: any) => {
      if ((input.address === escrow.address) && (input.redeemer.datum.bytes === distributeRedeemerBytes)) {
        distributionTxs += 1
      }
    })
  })

  // On mainnet the first distribution transaction was made to test the contracts.
  // (tx hash: 364db74ca115b265f966e6a595c9475a760bce91b296164a074daf2fe8131210)
  // Since this query uses redeemers instead of metadata keys for filtering the
  // transactions, these test cases must be removed manually.
  // IDEA: instead of managing this value manually we can relay on (guess what?)
  // metadata to signal that a specific transaction doesn't correspond for example to
  // a completed project and should not be dispayed as such on the front end.
  if (escrow.address === "addr1w9k23vumcqy08a8jqjcuewcq8ypkyamvmy5wy3fpjv378mq3mnwyq") {
    distributionTxs -= 1
  }
  return distributionTxs
}


const DistributionCountWidget = () => {

  const { data, loading, error } = useQuery(TRANSACTION_FROM_ESCROW_REDEEMERS_QUERY, {
    variables: {
      escrowAddress: escrow.address,
    },
  });

  if (loading) {
    return (
      <Center p="10">
        <Spinner size="xl" speed="1.0s" />
      </Center>
    );
  }

  if (error) {
    console.error(error);
    return <Heading size="lg">Error loading data...</Heading>;
  }

  return (
    <Box p="5" bg="gray.900" textAlign="center">
      <Heading size="3xl">{countDistributionTransaction(data.transactions)}</Heading>
      <Text>Completed Projects</Text>
    </Box>
  );
}

export default DistributionCountWidget;