import { escrow } from "../../cardano/plutus/escrowContract";
import { rawTxEscrow } from "../../types";
import { hexToString } from "../../cardano/utils";
import { treasury } from "../../cardano/plutus/treasuryContract";


export function filterTransactionsById(data : rawTxEscrow[], id : string) {
  let result : rawTxEscrow[] = []
  data.map((tx:rawTxEscrow) => (
    tx.metadata.map((txmetadata:any) => (
      txmetadata.value.id === id ?
        result.push(tx):null
    ))
  ))
  return result
}

export function getContributorTokenNames(data : rawTxEscrow[], toOrFrom : string) {
  let result : string[] = []
  data.map((tx:rawTxEscrow) => (
    toOrFrom === 'to' ?
      tx.outputs.map((output:any) => (
        output.address === escrow.address ?
          output.tokens.map((token:any) => (
            token.asset.policyId == treasury.accessTokenPolicyId ?
              result.push(hexToString(token.asset.assetName)):null              
          )):null
      ))
    :
      toOrFrom === 'from' ?
        tx.inputs.map((input:any) => (
          input.address === escrow.address ?
            input.tokens.map((token:any) => (
              token.asset.policyId == treasury.accessTokenPolicyId ?
                result.push(hexToString(token.asset.assetName)):null              
            )):null            
        ))
        :null
  ))
  return result
}

export function getCommitmentStatus(to:string[], from:string[], multipleCommitments:boolean): [string, string[]] {
    
  console.log(to)
  console.log(from)

  if (to.length == 0) {                     // No active commitments
    return ["Open for Commitment", []]
  }
  else if (to.length != from.length) {      // Active commitments live
    const activeTokens : string[] = []
    for (const tokenName of to) {
      if (!from.includes(tokenName)) {
        activeTokens.push(tokenName);
      }
    }
    if (multipleCommitments == true) { return ["Open for Commitment", activeTokens] }
    else { return ["Closed", activeTokens] }
  }
  else {                                    // Distributed all commitments
    if (multipleCommitments == true) { return ["Open for Commitment", []] }
    else { return ["Closed", []] }
  }
}
