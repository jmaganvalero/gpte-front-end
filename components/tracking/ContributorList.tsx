import Link from "next/link";
import { Box, Heading, Text, Link as ChakraLink } from "@chakra-ui/react";
import { getAllContributorTokenStatus } from "../../project-lib/utils";
import { EscrowTx } from "../../types";

type Props = {
  transactions: EscrowTx[];
};

const ContributorList: React.FC<Props> = ({ transactions }) => {
  const allContributors = getAllContributorTokenStatus(transactions);

  return (
    <Box>
      <Heading py="5">Escrow Transactions by Contributor Token:</Heading>
      <Text py="1" fontSize="xl">
        How can we use this data?
      </Text>
      <Text py="1" fontSize="xl">
        Try it:{" "}
        <Link href="/projects/0016">
          <ChakraLink>Project 0016</ChakraLink>
        </Link>
      </Text>
      <Box bg="white" color="black" fontSize="xs">
        <pre>
          <code className="language-js">
            {JSON.stringify(allContributors, null, 2)}
          </code>
        </pre>
      </Box>
    </Box>
  );
};

export default ContributorList;
