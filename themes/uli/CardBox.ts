import {chakra,Box} from "@chakra-ui/react";
const CardBox = chakra(Box, {
    baseStyle: {
      rounded: "lg",
      bg: "whiteAlpha.900",
      p: "2",
      mt: "2",
      mr: "2",
      ml: "2",
    },
  });
export default CardBox      